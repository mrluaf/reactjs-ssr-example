import express from "express";
import fs from "fs";
import path from "path";

import React from "react";
import ReactDOMServer from "react-dom/server";
import { Helmet } from "react-helmet";
import * as cheerio from "cheerio";

import App from "../src/App";

const PORT = 8437;

const app = express();

app.use("^/$", (req, res, next) => {
  fs.readFile(path.resolve("./build/index.html"), "utf-8", (err, data) => {
    if (err) {
      console.log(err);
      return res.status(500).send("Some error happened");
    }
    const app = ReactDOMServer.renderToString(<App />);
    const helmet = Helmet.renderStatic();
    const $ = cheerio.load(data);

    $('head').append(`${helmet.title.toString()}
    ${helmet.meta.toString()}
    ${helmet.link.toString()}`);
    const html = $.html().replace(
      '<div id="root"></div>',
      `<div id="root">${app}</div>`
    );

    res.send(html);

    // return res.send(
    //   data.replace(
    //     '<div id="root"></div>',
    //     `<div id="root">${app}</div>`
    //   )
    // );
  });
});

app.use(express.static(path.resolve(__dirname, '..', 'build')))

app.listen(PORT, () => {
  console.log(`App launched on ${PORT}`);
});
