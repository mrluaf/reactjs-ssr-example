import React from "react";
import { Helmet } from "react-helmet";
import "./App.css";

function App() {
  const [count, setCount] = React.useState(0);

  const increment = () => {
    setCount(count + 1);
  };

  const decrement = () => {
    setCount(count - 1);
  };

  return (
    <>
      <Helmet>
          <meta charSet="utf-8" />
          <title>SSR cho ReactJS - Nguyễn Văn Tài</title>
          <link rel="canonical" href="https://ssr-react.nguyenvantai.vn" />
          <meta name="description" content="Đây là website được code bằng ReactJS nhưng được cấu hình SSR bằng NodeJS. Có cấu hình một số thẻ Meta để tối ưu SEO" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <meta name="author" content="Nguyễn Văn Tài" />
          <meta name="twitter:card" content="summary" />
          <meta name="twitter:site" content="@nguyenvantai_vn" />
          <meta property="og:title" content="SSR cho ReactJS - Nguyễn Văn Tài" />
          <meta property="og:description" content="Đây là website được code bằng ReactJS nhưng được cấu hình SSR bằng NodeJS. Có cấu hình một số thẻ Meta để tối ưu SEO" />
          <meta property="og:type" content="article" />
          <meta property="og:url" content="https://ssr-react.nguyenvantai.vn" />
          <meta property="og:image" content="https://nguyenvantai.vn/wp-content/uploads/2022/03/nguyenvantai-avatar-550x825.jpg" />
          <meta property="og:image:width" content="200" />
          <meta property="og:image:height" content="200" />
          <meta property="og:site_name" content="SSR React - Nguyễn Văn Tài" />
      </Helmet>
      <h3>Xin chào. Đây là website được phát triển bằng ReactJS và được cấu hình SSR với NodeJS</h3>
      <p>{count}</p>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
      <p></p>
      <center> Made with <span role="img" aria-label="heart">❤️</span> by <span><a href='https://nguyenvantai.vn' target='_blank'>Nguyễn Văn Tài</a></span><span><br></br><a href='https://gitlab.com/mrluaf/reactjs-ssr-example' target='_blank'> Get Source</a></span>
      </center>
    </>
  );
}

export default App;
